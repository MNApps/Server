from flask import send_file, url_for
from flask_security import UserMixin, RoleMixin
from peewee import (
        CharField, TextField, BooleanField, DateTimeField, ForeignKeyField,
        UUIDField
)
from playhouse.flask_utils import get_object_or_404
from playhouse.sqlite_ext import JSONField

from . import flask_db


''' Client Classes '''


class Client(flask_db.Model):
    name = TextField()
    subdomain = TextField()

    def url_for(self, func, *args, **kwargs):
        return url_for(func, *args, client=self.subdomain, **kwargs)

    @staticmethod
    def get_or_404(client):
        cli = get_object_or_404(Client.select(),
                                Client.subdomain == client)
        return cli


''' User Classes '''


class Role(flask_db.Model, RoleMixin):
    name = CharField(unique=True)
    description = TextField(null=True)


class User(flask_db.Model, UserMixin):
    name = TextField()
    email = TextField(unique=True)
    password = TextField()
    active = BooleanField(default=True)
    confirmed_at = DateTimeField(null=True)
    client = ForeignKeyField(Client, related_name='users', null=True)


class UserRoles(flask_db.Model):
    # Intermediary class to link user to roles (many to many)
    user = ForeignKeyField(User, related_name='roles')
    role = ForeignKeyField(Role, related_name='users')
    name = property(lambda self: self.role.name)
    description = property(lambda self: self.role.description)


''' Media Classes '''


class Media(flask_db.Model):
    uuid = UUIDField(index=True, null=False, unique=True)
    path = TextField(null=False)
    mimetype = CharField(null=False)

    def render(self):
        return send_file(self.path, mimetype=self.mimetype)


''' Page Classes '''


class Template(flask_db.Model):
    name = TextField()


class Page(flask_db.Model):
    template = ForeignKeyField(Template, related_name='templates')
    client = ForeignKeyField(Client, related_name='pages')
    icon = ForeignKeyField(Media, related_name='references', null=True)
    title = TextField()
    data = JSONField()
