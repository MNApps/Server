from flask import Blueprint, g, abort
from flask_restful import Resource, Api

from .. import app
from ..models import Client, Page


api_bp = Blueprint('api', __name__, subdomain='<client>', url_prefix='/api')
api = Api(api_bp)


# app.logger.debug(dir(api))


@api_bp.url_value_preprocessor
def check_client_and_user(endpoint, values):
    g.client = values.pop('client')


@api_bp.before_request
def check_client():
    g.client = Client.get_or_404(g.client)


class Pages(Resource):
    def get(self):
        for page in g.client.pages:
            return {
                    'title': page.title,
                    'id': page.id}


class APage(Resource):
    def get(self, page_id=None):
        if not page_id:
            abort(404)
        try:
            page = g.client.pages.where(Page.id == page_id).get()
        except Page.DoesNotExist:
            abort(404)
        return {
                'title': page.title,
                'icon': None if not page.icon else page.icon.uuid,
                'template': {
                    'name': page.template.name
                },
                'data': page.data
        }

    def post(self, page_id=None):
        # TODO: Add api for adding pages.
        pass


api.add_resource(Pages, '/pages')
api.add_resource(APage, '/page', '/page/<page_id>')
