from flask_wtf import FlaskForm
import json
from wtforms import StringField, TextAreaField, SelectField, FileField
from wtforms.validators import data_required

from .models import Template, Page


class FillForm(FlaskForm):
    def __init__(self, obj):
        super().__init__()
        if obj:
            self.fill_data(obj)

    def fill_data(self, obj):
        raise NotImplemented()


class PageForm(FillForm):
    form_template = 'panel/forms/form.html'

    title = StringField('Titel')
    template = SelectField('Template',
                           choices=[(t.id, t.name) for t in Template.select()])
    icon = FileField('Icoon')
    data = TextAreaField('data')

    def fill_data(self, obj):
        self.title.data = obj.title
        self.template.data = obj.template.id
        self.data.data = json.dumps(obj.data, indent=4, separators=(',', ': '))
