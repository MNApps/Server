import os

import click
from flask import Flask
from flask_security import (
        Security, PeeweeUserDatastore, login_required, current_user
)
from playhouse.flask_utils import FlaskDB, get_object_or_404


app = Flask(__name__)


SITE_NAME = 'MNApps'
SERVER_NAME = 'flask.dev:5000'
DEBUG = True
SECRET_KEY = 'Keep_This_Secret!'
CSRF_ENABLED = True
CSRF_SESSION_KEY = 'This_Too!'

SECURITY_PASSWORD_SALT = 'Also Secret...'

UPLOAD_PATH = os.path.join(app.instance_path, 'uploads/')
DATABASE = 'sqlite:///{}'.format(os.path.join(app.instance_path,
                                              'database.db'))

app.config.from_object(__name__)

flask_db = FlaskDB(app)
database = flask_db.database

from .panel import panel
from .admin import admin
from .api import api_bp

app.register_blueprint(panel)
app.register_blueprint(admin)
app.register_blueprint(api_bp)


from .models import Client, Role, User, UserRoles, Media, Template, Page

user_datastore = PeeweeUserDatastore(flask_db, User, Role, UserRoles)
security = Security(app, user_datastore)


@app.route('/')
@login_required
def index():
    return 'Hi!'


@app.route('/media/<media_uuid>')
def media(media_uuid):
    res = get_object_or_404(Media.select(), Media.uuid == media_uuid)
    return res.render()


@app.cli.command()
def init():
    click.echo('Initiating project.')

    for Model in (Client, Role, User, UserRoles, Media, Template, Page):
        Model.drop_table(fail_silently=True)
        Model.create_table(fail_silently=True)

    template = Template(name='info_page')
    template.save()

    client = Client()
    client.name = 'Test'
    client.subdomain = 'test'
    client.save()

    page = Page(template=template, client=client, title='Over ons',
                data=[
                    {'p': 'This is some random paragraph', 'align': 'left'},
                    {'p': 'Another paragraph', 'align': 'center'}
                ])
    page.save()

    role = user_datastore.create_role(name='SuperUser',
                                      description='Ruler of the world')
    user = user_datastore.create_user(name='Marten',
                                      email='test@marten.xyz',
                                      password='password',
                                      role='SuperUser', client=client)
    user_datastore.add_role_to_user(user, role)
