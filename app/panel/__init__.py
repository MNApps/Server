from collections import namedtuple

from flask import Blueprint, g, abort, render_template, redirect, url_for
from flask_security import login_required, current_user
from playhouse.flask_utils import get_object_or_404

from .. import app, database
from ..forms import PageForm
from ..models import Client, Page


panel = Blueprint('panel', __name__, subdomain='<client>')


@panel.url_value_preprocessor
def set_g_client(endpoint, values):
    g.client = values.pop('client')


@panel.before_request
def check_client_and_user():
    g.client = Client.get_or_404(g.client)
    g.panel_name = g.client.name
    if not current_user.is_authenticated:
        return redirect(url_for('login'))
    if current_user not in g.client.users or\
       not current_user.has_role('SuperUser'):
        abort(403)

    g.panel_pages = []

    PanelPage = namedtuple('PanelPage', ['name', 'title', 'forms'])
    g.panel_pages.append(PanelPage('dashboard', 'Dashboard', None))
    g.panel_pages.append(PanelPage('pages', 'Pagina\'s', None))


@panel.route('/')
@login_required
def dashboard():
    return render_template('panel/panel.html', current_page='dashboard')


@panel.route('/pages')
def pages():
    return render_template('panel/pages.html', current_page='pages')


@panel.route('/page/<page_id>')
def edit_page(page_id):
    page = Page.get(Page.id == page_id)
    forms = [
            PageForm(page)
    ]
    return render_template('panel/forms.html', forms=forms,
                           panel_title='Pagina \'{}\' bewerken'.
                                       format(page.title))


@panel.route('/push')
def push():
    '''
    forms = []
    forms.append(PushForm(), NotifForm())
    render render_template('panel/forms.html', forms=forms)

    {% for form %}
        {% include form.form_template %}
    {% endfor %}
    '''
    pass
