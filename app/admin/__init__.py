from flask import Blueprint, abort, render_template, g
from flask_security import login_required, current_user
from .. import app


admin = Blueprint('admin', __name__, subdomain='admin')


@admin.before_request
def check_user():
    if not current_user.is_authenticated:
        return app.login_manager.unauthorized()
    # app.logger.debug(current_user.roles.get())
    if not current_user.has_role('SuperUser'):
        abort(403)
    g.panel_name = 'Admin Panel'


@admin.route('/')
def index():
    return render_template('panel/panel.html')
